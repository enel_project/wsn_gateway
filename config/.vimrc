" Turns on line number
set number relativenumber
"
set hlsearch

" Clear out search

inoremap jk <ESC>
noremap <Enter> <S-o><Esc>j
set laststatus=2                " Always show the status line
" set statusline=\ %F%m%r%h\ %w\ \ CWD:\ %r%{getcwd()}%h\ \ \ Line:\ %l " Format the status line
"
"
set splitright " Open splits to the right

nnoremap j gj
nnoremap k gk
"
"

nnoremap <leader><space> :noh<cr>
" Disable Ex mode

syntax on

noremap Q <NOP>
" Sets color mode
:color desert
" Turns on filetype options

filetype plugin indent on
" show existing tab with 2 spaces width
set expandtab                   " Mandatory! Expand tab to spaces
set shiftwidth=2
set tabstop=2
set softtabstop=2
" Map Ctrl-<j|k|l|h> to change focused window split
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l
" Disable arrow keys in command/visual mode
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
" Opens header file of current file in new tab (Oph from command line)
function! OpenHeader()
if (&ft=='cpp')
	:vsp %<.h
else
  :vsp %<.cpp
endif
endfunction
nnoremap <leader>h :call OpenHeader()<cr>
