
import threading
import signal
import time
import requests
import sqlite3
import serial
import sys

import wsn_module
from wsn_module import wsn

import cloud_module
from cloud_module import cloud

sqlite3_m = threading.Lock()
ready_e = threading.Event()

def sig_handler(signal, frame):
    print("Interrupted")
    sys.exit(0)

wsn_t = threading.Thread(name="wsn thread", target = wsn.run, args = (sqlite3_m,ready_e,))
cloud_t = threading.Thread(name="cloud thread", target = cloud.run, args = (sqlite3_m,ready_e))

wsn_t.daemon = True;
cloud_t.daemon = True;

wsn_t.start()
cloud_t.start()

signal.signal(signal.SIGINT, sig_handler)

while True:
    print("Opening database")
    print('Threads started')
    print('Waiting on signals...')
    signal.pause()

