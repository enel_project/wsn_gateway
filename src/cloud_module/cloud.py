
import time
import requests
import sqlite3
import json

def db_connect(db_name):
    try:
        return sqlite3.connect(db_name)
    except sqlite3.Error as e:
        return None
        print(e)

def get_controls():
    headers = {'referer' : 'gateway'}
    response = requests.get(url='http://keg-monitor.com/api/wsn',
                            headers=headers)
    return response.status_code

def consume_controls(controls):
    return controls

def get_data(db):
    data = []
    cur = db.cursor()
    try:
        try: 
            cur.execute("SELECT mac, voltage, temperature, volume, pressure, timestamp FROM Data ORDER BY mac") 
        except sqlite3.Error as e: 
            print(e)
            return 0

        rows = cur.fetchall()
        if (rows):
            for row in rows:
                data.append({
                    "keg" : row[0],
                    "voltage" : row[1],
                    "temperature" : row[2],
                    "level" : row[3],
                    "pressure" : row[4],
                    "timestamp" : row[5]
                });
    finally:
        cur.close()
        return data

def del_data(db):
    cur = db.cursor()
    try:
        cur.execute("DELETE FROM Data")
        db.commit()
    except sqlite3.Error as e:
        print(e)
    finally:
        cur.close()

def post_data(data):
    headers = {'referer' : 'gateway'}
    
    response = requests.put(url='http://keg-monitor.com/api/wsn', 
                            headers=headers,
                            json=data)
    print(response.text)
    return response.status_code

def run(sqlite3_m, ready_e):
    ticks = time.time()
    delay = 600
    db = db_connect('/home/pi/wsn_gateway/wsn.db')
    dbm = db_connect(':memory:')
    while True:
        data = []
        if (time.time() > (ticks + delay)):
            print(consume_controls(get_controls()))
            ticks = time.time()

        ready_e.wait(300)
        sqlite3_m.acquire()
        data = get_data(db)
        if (data):
            rs = post_data(data)
            print(rs)
            del_data(db)
        sqlite3_m.release()
        ready_e.clear()

