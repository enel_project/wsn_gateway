
import sqlite3
import serial
import time
from enum import Enum

class Response(Enum):
    ACK = 0
    NACK = 1
    DACK = 2
    CONT = 3
    
ACK = bytes.fromhex('03A0A3')
NACK = bytes.fromhex('03B0B3')
DACK = bytes.fromhex('')
CONT = bytes.fromhex('0DD013A2004177EC865802008E')

def decode_START(port, packet):
    response = Response.ACK
    try:
        length = int(packet[0], 16)
        MAC = ''.join(x[2:] for x in packet[2:9])
        packets = int(packet[9], 16)
        time = int(''.join(x[2:] for x in packet[11:9:-1]), 16)

        print("\nSTART Packet recieved")
        print("from MAC: " + MAC)
        print(str(packets) + " packets to recieve")
        print(str(time) + " seconds since last callin")

        respond(port, Response.ACK)
    except:
        print("Packet Incorrect")
        respond(port, Response.NACK)
    
    # TODO
    # Check schedule to see if node is supposed to be calling in
    # Return DACK with return to sleep code if not it's turn, ACK otherwise

def decode_DATA(port, packet):
    response = Response.ACK
    try:
        length = int(packet[0], 16)
        MAC = ''.join(x[2:] for x in packet[2:9])
        packet_number = int(packet[9], 16)
        data_samples = int(packet[10], 16)
        data = []
        for i in range(0,data_samples):
            data_type = packet[10 + 5*i + 1]
            data_value = int(''.join(x[2:] for x in packet[13 + 5*i:11 + 5*i:-1]), 16)
            time = int(''.join(x[2:] for x in packet[15 + 5*i:13 + 5*i:-1]), 16)
            data.append({'type' : data_type, 'mac' : MAC, 'data' : data_value, 'time' : time})

        print("\nDATA Packet recieved")
        print("from MAC: " + MAC)
        print("Packet Number: " + str(packet_number))
        print(str(data_samples) + " samples to recieve")
        print(data)

        respond(port, Response.ACK)
        return {"mac" : MAC, "packet" : "DATA", "data" : data}

    except:
        print("Packet Incorrect")
        respond(port, Response.NACK)
        return 0 

def decode_GET(port, packet):
    response = Response.ACK
    try:
        length = int(packet[0], 16)
        MAC = ''.join(x[2:] for x in packet[2:9])
        response = Response.CONT

        print("\nGET Packet recieved")
        print("from MAC: " + MAC)
        print("Sending control information")

        respond(port, Response.CONT)
    except:
        print("Packet Incorrect")
        respond(port, Response.NACK)

    # TODO 
    # Grab config information from database
    # Format config information into CONT packet and send

def decode_END(port, packet):
    response = Response.ACK
    try:
        length = int(packet[0], 16)
        MAC = ''.join(x[2:] for x in packet[2:9])

        print("\nEND Packet recieved")
        print("from MAC: " + MAC)
        print("Ending communications")

        respond(port, Response.ACK)
        return {"mac" : MAC, "packet" : "END"}
    except:
        print("Packet Incorrect")
        respond(port, Response.NACK)
        return 0

decode_switch = {
        '0x10' : decode_START,
        '0x20' : decode_DATA,
        '0x30' : decode_GET,
        '0x40' : decode_END
}

def respond(port, response):
    port.flush()
    time.sleep(0.01);
    if (response == Response.ACK):
        print("Sending ACK")
        port.write(ACK)
        print(ACK)
    elif (response == Response.NACK):
        print("Sending NACK")
        port.write(NACK)
        print(NACK)
    elif (response == Response.DACK):
        print("Sending DACK")
        port.write(DACK)
        print(DACK)
    elif (response == Response.CONT):
        print("Sending CONT")
        port.write(CONT)
        print(CONT)

def db_connect(db_name):
    try:
        return sqlite3.connect(db_name)
    except sqlite3.Error as e:
        return None
        print(e)

def store_readings(db, sensors):
    cur = db.cursor()   
    sleep_time = 30
    data = []
    voltage = []
    temperature = []
    pressure = []
    volume = []
    for reading in sensors:
        if not any(data_reading["offset"] == reading["time"] for data_reading in data):
            data.append({"mac" : int(reading["mac"], 16), "offset" : reading["time"], 
                         "volume" : 0, "temperature" : 0, "voltage" : 0, "pressure" : 0})
         
        if reading["type"] == '0x31':
            index = next((index for (index, d) in enumerate(data) if d["offset"] == reading["time"]), None)
            data[index]["volume"] = reading["data"]
        elif reading["type"] == '0x32':
            index = next((index for (index, d) in enumerate(data) if d["offset"] == reading["time"]), None)
            data[index]["voltage"] = reading["data"]
        elif reading["type"] == '0x33':
            index = next((index for (index, d) in enumerate(data) if d["offset"] == reading["time"]), None)
            data[index]["temperature"] = reading["data"]
        elif reading["type"] == '0x34':
            index = next((index for (index, d) in enumerate(data) if d["offset"] == reading["time"]), None)
            data[index]["pressure"] = reading["data"]
        else:
            pass

    try:
        cur.execute("SELECT last_callin, sleep_time FROM Config WHERE mac=%s" % (data[0]["mac"]))
        row = cur.fetchone()
        print(row)
        if (row):
            last_callin = row[0]
            sleep_time = row[1]
        else:
            next_callin = 30
            last_callin = 0

        for reading in data:

            cur.execute("""INSERT INTO Data 
                           (mac, voltage, temperature, volume, pressure, timestamp) 
                           VALUES 
                           (%s, %s, %s, %s, %s, 
                           DATETIME((SELECT last_callin FROM Config WHERE mac=%s), '+%s seconds'));""" % 
                           (reading["mac"],
                            reading["voltage"],
                            reading["temperature"],
                            reading["volume"],
                            reading["pressure"],
                            reading["mac"],
                            reading["offset"]))
        
        db.commit()
    except sqlite3.Error as e:
        print("SQLite3 Error:")
        print(e)
    cur.close()
    return sleep_time

def set_last_callin(db, mac):
    cur = db.cursor()   
    cur.execute("UPDATE Config SET last_callin=CURRENT_TIMESTAMP WHERE mac=%s" % (int(mac, 16)))
    db.commit()
    cur.close()

def calculate_checksum(packet, length):
    checksum = 0
    for i in range(0, length - 1):
        checksum += int(packet[i], 16)
    return (checksum & 0xFF)

def check_length(packet):
    return (int(packet[0], 16) == len(packet))

def check_checksum(packet):
    length = len(packet)
    checksum = int(packet[length - 1], 16)
    return (calculate_checksum(packet, length) == checksum)

def decode(port, packet):
    global decode_switch
    switch = decode_switch.get(packet[1], lambda x: (print("Invalid packet: " + str(x[1]))))
    return switch(port, packet)
    
def listener(port):
    data = []
    packet = []

    # Check for data and read in if exists
    if (port.in_waiting):
        length = port.read(1).hex()
        rx = length + port.read(int(length, 16) - 1).hex()
        packet = ['0x{:02X}'.format(int(rx[i:i+2], 16)) for i in range(0, len(rx), 2)]
        if (port.in_waiting):
            response(port, Response.NACK);
            packet = []

        # Flush serial port
        port.flush()


    # If packet exists, test length and checksum
    if (packet):
        length_passed = False
        checksum_passed = False
        print("\nPacket Recieved:")
        print(packet)

        # Test Length
        if check_length(packet):
            length_passed = True
        else:
            print("Length invalid")

        # Test Checksum
        if check_checksum(packet):
            checksum_passed = True
        else:
            print("Checksum invalid")

        # Decode packet if good
        if (length_passed and checksum_passed):
            data = decode(port, packet)
        else:
            respond(port, Response.NACK)

    return data

def run(sqlite3_m, ready_e):
    sleep = 30
    db = db_connect('/home/pi/wsn_gateway/wsn.db')
    dbm = db_connect(':memory:')
    db.set_trace_callback(print)
    ser = serial.Serial()
    try:
        ser.port = '/dev/ttyUSB0'
        ser.baudrate = 9600
        ser.timeout = 5
        ser.open()
    except:
        print("Serial Error")
    
    while True:
        data = []
        data = listener(ser)
        if (data and data["packet"] and data["packet"] == "END"):
            sqlite3_m.acquire() 
            set_last_callin(db, data["mac"])
            sqlite3_m.release()
        if (data and data["packet"] and data["packet"] == "DATA"):
            sqlite3_m.acquire() 
            sleep = store_readings(db, data["data"]);
            sqlite3_m.release()
            ready_e.set()
