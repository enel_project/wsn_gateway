
import sqlite3
from subprocess import call


def db_connect(db_name):
    db = 0
    try:
        db = sqlite3.connect(db_name)
    except sqlite3.Error as e:
        print(e)
    finally:
        return db

def exec(db, sql):
    try:
        c = db.cursor()
        c.execute(sql)
    except sqlite3.Error as e:
        print(e)

def main():

    call(['rm', 'wsn.db'])

    DATABASE = 'wsn.db'
    db = db_connect(DATABASE)
    
    exec(db, "PRAGMA foreign_keys = 1");

    SQL_CREATE_TABLE = []

    SQL_CREATE_TABLE.append({
        "name": "Config",    
        "desc": "Node Configuration Information",
        "sql":  """CREATE TABLE IF NOT EXISTS Config (
                    mac INTEGER NOT NULL PRIMARY KEY,
                    last_callin DATETIME DEFAULT CURRENT_TIMESTAMP,
                    sleep_time INTEGER DEFAULT 30,
                    timestamp DATETIME DEFAULT CURRENT_TIMESTAMP
                 );""" })
    SQL_CREATE_TABLE.append({
        "name": "Data",
        "desc": "Nodes Sensor Readings",
        "sql": """CREATE TABLE IF NOT EXISTS Data (
                    id INTEGER PRIMARY KEY,
                    mac INTEGER NOT NULL,
                    voltage INTEGER DEFAULT 0,
                    temperature INTEGER DEFAULT 0,
                    volume INTEGER DEFAULT 0,
                    pressure INTEGER DEFAULT 0,
                    timestamp DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                    FOREIGN KEY (mac) REFERENCES Config (mac)
                 );""" })

    for table in SQL_CREATE_TABLE:
        print("Creating table " + table["name"] + ": " + table["desc"])
        exec(db, table["sql"])


if __name__ == '__main__':
    main()
