#!/bin/bash

echo
echo "Installing required packages"
echo
apt install rsyslog sqlite3 python3-pip vim

echo
echo "Building database"
echo

python3 create_db.py
chown pi wsn.db
chmod 666 wsn.db

echo 
echo "Creating log files"
echo

mkdir /home/pi/logs
touch /home/pi/logs/gateway.log

echo
echo "Setting up reverse tunnel"
echo

touch cronjobs
echo "*/1 * * * * ssh -i /home/pi/.ssh/gateway -o TCPKeepAlive=yes -o ServerAliveInterval=10 -f -R 15001:localhost:22 brantgeddes@keg-monitor.com \"touch ~/gateway/$HOSTNAME ; echo 'ssh -i /home/brantgeddes/.ssh/rtunnel pi@localhost -p 15001' > ~/gateway/$HOSTNAME ; sleep 295 ; rm ~/gateway/$HOSTNAME \"" >> cronjobs
crontab -u pi cronjobs
rm cronjobs

echo
echo "Setting up gateway service"
echo

cp config/gateway.service /lib/systemd/system/gateway.service
cp config/gateway.conf /etc/rsyslog.d/gateway.conf
sudo systemctl restart rsyslog
systemctl daemon-reload
systemctl enable gateway
systemctl restart gateway

echo
echo "Moving Config files"
echo

cp config/.vimrc ~/.vimrc
cp config/.inputrc ~/.inputrc

echo
echo "Installation Complete"
echo
